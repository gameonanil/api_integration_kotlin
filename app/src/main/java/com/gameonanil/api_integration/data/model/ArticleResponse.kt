package com.gameonanil.api_integration.data.model

data class ArticleResponse(
    val articles: List<Article>,
)