package com.gameonanil.api_integration.data.network

import com.gameonanil.api_integration.data.foodmodel.FoodsItem
import com.gameonanil.api_integration.data.model.Restaurant
import retrofit2.http.GET

interface FoodApi {

    @GET("api/foods")
    suspend fun getFood(
    ): List<FoodsItem>

    @GET("api/restaurants")
    suspend fun getRestaurant(
    ): List<Restaurant>
}