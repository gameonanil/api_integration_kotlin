package com.gameonanil.api_integration.data.foodmodel

data class FoodsItem(
    val address: String,
    val available: String,
    val id: Int,
    val image: String,
    val name: String,
    val price: Int,
    val rating: Float
)