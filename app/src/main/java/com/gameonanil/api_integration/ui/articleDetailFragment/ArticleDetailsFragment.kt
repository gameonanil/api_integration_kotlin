package com.gameonanil.api_integration.ui.articleDetailFragment

import android.os.Bundle
import android.transition.TransitionInflater
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import coil.compose.rememberImagePainter
import com.gameonanil.api_integration.databinding.FragmentArticleDetailsBinding


class ArticleDetailsFragment : Fragment() {

    private var _binding: FragmentArticleDetailsBinding? = null
    private val binding: FragmentArticleDetailsBinding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentArticleDetailsBinding.inflate(inflater, container, false)


        val animation =
            TransitionInflater.from(requireContext()).inflateTransition(android.R.transition.move)
        sharedElementEnterTransition = animation
        sharedElementReturnTransition = animation

        val title = ArticleDetailsFragmentArgs.fromBundle(requireArguments()).title
        val description = ArticleDetailsFragmentArgs.fromBundle(requireArguments()).description
        val author = ArticleDetailsFragmentArgs.fromBundle(requireArguments()).author
        val content = ArticleDetailsFragmentArgs.fromBundle(requireArguments()).content
        val url = ArticleDetailsFragmentArgs.fromBundle(requireArguments()).url

        binding.composeView.setContent {
            ArticleFragmentComposable(title, description, author, content, url = url)
        }



        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()

    }


    @Composable
    fun ArticleFragmentComposable(
        title: String,
        description: String,
        author: String,
        content: String,
        url: String
    ) {
        Box(modifier = Modifier.fillMaxWidth()) {

            Image(
                painter = rememberImagePainter(url),
                "imageViewItem",
                modifier = Modifier
                    .fillMaxWidth()
                    .height(350.dp),
                contentScale = ContentScale.Crop
            )
            Scaffold(
                modifier = Modifier.fillMaxSize(),
                topBar = {
                    TopAppBar(
                        backgroundColor = Color.Transparent,
                        elevation = 0.dp,
                        modifier = Modifier.padding(top = 14.dp)
                    ) {
                        Spacer(modifier = Modifier.height(230.dp))
                        Surface(
                            shape = CircleShape,
                            color = Color.White.copy(0.6f),
                            modifier = Modifier.size(40.dp)
                        ) {
                            IconButton(onClick = { findNavController().popBackStack() }) {
                                Icon(Icons.Default.ArrowBack, "", tint = Color.Black)
                            }
                        }
                    }
                },
                backgroundColor = Color.Transparent
            ) {
                Column {
                    Spacer(modifier = Modifier.height(230.dp))
                    Surface(
                        shape = RoundedCornerShape(topEnd = 30.dp, topStart = 30.dp),
                        modifier = Modifier
                            .fillMaxSize(),
                        color = Color.White,
                    ) {
                        Column(modifier = Modifier.padding(15.dp)) {
                            Text(
                                text = author,
                                style = TextStyle(
                                    color = Color.Black,
                                    fontWeight = FontWeight.Bold,
                                    fontSize = 30.sp,
                                ),
                                modifier = Modifier
                                    .align(Alignment.CenterHorizontally)
                                    .fillMaxWidth()
                            )
                            Text(
                                text = description,
                                style = TextStyle(
                                    color = Color.Black,
                                    fontSize = 20.sp,
                                ),
                                modifier = Modifier
                                    .align(Alignment.CenterHorizontally)
                                    .fillMaxWidth()
                            )
                            Text(
                                text = content,
                                style = TextStyle(
                                    color = Color.Black,
                                    fontSize = 20.sp,
                                ),
                                modifier = Modifier
                                    .align(Alignment.CenterHorizontally)
                                    .fillMaxWidth()
                            )
                        }

                    }
                }
            }
        }


    }


}


