package com.gameonanil.api_integration.utils

data class Resource<out T> (val status:Status,val data:T?,val exception: AppException?){
    public enum class Status{
        LOADING,
        SUCCESS,
        ERROR
    }

    companion object {
        fun <T> success(data: T) : Resource<T> = Resource(Status.SUCCESS,data, null)

        fun <T> error(data: T?, exception: AppException?): Resource<T> = Resource(Status.ERROR, data, exception)

        fun <T> loading(data:T?):Resource<T> = Resource(Status.LOADING,data,null)
    }



}